'use strict';

// Let's GO!
var gulp         = require('gulp'),
    browserSync  = require('browser-sync').create(),
    reload       = browserSync.reload,
    imagemin     = require('gulp-imagemin'),
    pngquant     = require('imagemin-pngquant'),
    sass         = require('gulp-sass'),
    useref       = require('gulp-useref'),
    gulpif       = require('gulp-if'),
    uglify       = require('gulp-uglify'),
    minifyCss    = require('gulp-minify-css'),
    autoprefixer = require('gulp-autoprefixer'),
    jade         = require('gulp-jade'),
    sourcemaps   = require('gulp-sourcemaps');


// --------------------------------------------------------
// LiveReload with BrowserSync
// --------------------------------------------------------
gulp.task('serve', ['sass'], function() {
    browserSync.init({
        server: {
            baseDir: ['app/templates', 'app']
        },
        //tunnel: "veedli", //open external access to this server
        //host: 'metaform-lab',
        open: "local",
        browser: "google chrome canary",
        port: 3000,
        online: true, // if you're working offline set to false this item retuce startUp time
        logPrefix: "MetaformLab"

    });
    //set watcher
    gulp.watch('app/templates/**/*.html').on('change', browserSync.reload);
    gulp.watch('app/scripts/**/*.js').on('change', browserSync.reload);
    gulp.watch('app/styles/css/*.css').on('change', browserSync.reload);

    gulp.watch(['app/styles/sass/**/*.scss'], ['sass']);
});


// --------------------------------------------------------
// Compile SASS & CSS autoprefixer
// --------------------------------------------------------

gulp.task('sass', function () {
  gulp.src('app/styles/sass/**/*.scss')
    .pipe(sourcemaps.init())
      .pipe(sass.sync().on('error', sass.logError))
      .pipe(autoprefixer({
        browsers: ['last 10 versions'],
        cascade: true
    }))
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(sourcemaps.write())
    //.pipe(browserSync.stream())
    .pipe(gulp.dest('app/styles/css'));
});

// --------------------------------------------------------
// Optimize images & copy to distribution folder 'dist'
// --------------------------------------------------------
gulp.task('image', function () {
    return gulp.src('app/img/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('dist/img'));
});

// --------------------------------------------------------
// Copy JS dependens to dist
// --------------------------------------------------------
gulp.task('copyDep', function() {
    gulp.src(['app/scripts/vendor/**/*'])
        .pipe(gulp.dest('dist/scripts/vendor'));
});

// --------------------------------------------------------
// Copy general files
// --------------------------------------------------------
gulp.task('copy', function() {
  gulp.src(['app/browserconfig.xml',
            'app/crossdomain.xml',
            'app/humans.txt',
            'app/robots.txt',
            'app/tile.png',
            'app/tile-wide.png' ])
    .pipe(gulp.dest('dist'));
});

// --------------------------------------------------------
// Concat CSS & JS
// --------------------------------------------------------
gulp.task('concat', function () {
    var assets = useref.assets();

    return gulp.src('app/templates/index.html')
        .pipe(assets)
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', minifyCss()))
        .pipe(assets.restore())
        .pipe(useref())
        .pipe(gulp.dest('dist'));
});

// --------------------------------------------------------
// Prepare files for PRODUCTION
// --------------------------------------------------------
gulp.task('dist', ['concat', 'image', 'copyDep', 'copy']);

// --------------------------------------------------------
// Create production ver. and Run!
// --------------------------------------------------------
gulp.task('serve:dist', ['dist'], function() {
    browserSync.init({
        server: {
            baseDir: ['dist']
        }
    });
});

